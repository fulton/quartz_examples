﻿using System;
using Quartz;

namespace quartz_client
{
    class Program
    {
        static void Main(string[] args)
        {
            var schedule = SchedulerManager.GetScheduler("127.0.0.1");
            Type jobType = FakeJob.CreateJobType("PreSummarizationTicks.Service", "PreSummarizationTicks.Service.Job.PresummarizationJob");
            ScheduleJob(schedule, jobType, "test", "testremootetrigger", "testremotegroup", 60);
        }
        public static void ScheduleJob(IScheduler scheduler, Type jobType, string jobname, string triggername, string groupname, int second)
        {
            var datamap = new JobDataMap
            {
                {"DatabaseNumber", 0},
                {"SourceGranularity", 1},
                {"SummarizeGranularity", 60},
                {"MarketNumber", "B"},
                {"ShouldUpdateTimeStamp", "true"}
            };

            IJobDetail job = JobBuilder.Create(jobType)
                  .WithIdentity(jobname, groupname)
                  .SetJobData(datamap)
                  .Build();

            // 每秒执行的触发器
            ITrigger trigger = TriggerBuilder.Create()
              .WithIdentity(triggername, groupname)
              .StartNow()
              .WithSimpleSchedule(x => x
              .WithIntervalInSeconds(second)
              .RepeatForever())
              .Build();
            scheduler.ScheduleJob(job, trigger);
        }
    }
}
